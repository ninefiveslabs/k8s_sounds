import kopf


@kopf.on.create("eventer.kopf", "v1", "eventers")
def create(body, **kwargs):
    print(f"Create with spec: {body['spec']}")
    kopf.event(body, type="Important", reason="Action", message=f"Create with spec: {body['spec']}")
    return {"message": f"Create {body['spec']['name']}!"}


@kopf.on.update("eventer.kopf", "v1", "eventers")
def update(body, new, old, **kwargs):
    print(f"Update with spec: {body}")
    print(f"New: {new}")
    print(f"Old: {old}")
    kopf.event(body, type="Important", reason="Action", message=f"Update\nNew: {new}"
                                                                f"\nOld: {old}")
    kopf.info(body, reason="SomeReason", message="Some message")
    return {"message": f"Update {body['spec']['name']}!"}


@kopf.on.delete("eventer.kopf", "v1", "eventers")
def delete(body, **kwargs):
    print(f"Delete with spec: {body}")
    kopf.event(body, type="Normal", reason="Logging", message=f"Delete with spec: {body}")
    return {"message": f"Delete {body}"}