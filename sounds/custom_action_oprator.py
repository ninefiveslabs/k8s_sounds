import kopf
import pykube
import yaml
import uuid
import json

SERVER = ''
@kopf.on.create("", "v1", "pods")
def create(body, **kwargs):
    print(f"POD created")
    pod_name = body['metadata']['name']
    uid = str(uuid.uuid4())[:4]
    job_name = pod_name + uid
    if not 'sender' in pod_name:
        print(f"CREATE  JOB for this pod")
        send(job_name, SERVER)
        kopf.event(body, type="Sender", reason="Send", message=f"Send {pod_name} to {SERVER} ")


def send(job_name, server):
    data = f"name={job_name}"
    pod_data = yaml.safe_load(f'''
        apiVersion: batch/v1
        kind: Job
        metadata:
          name: sender-{job_name}
        spec:
          template:
            spec:
              containers:
              - name: sender-{job_name}
                image: byrnedo/alpine-curl
                command: ["/usr/bin/curl"]
                args: ['-X POST', 'http://192.168.99.105:8080/pod', '-d', '{data}']
              restartPolicy: Never
    ''')

    # Actually create an object by requesting the Kubernetes API.
    api = pykube.HTTPClient(pykube.KubeConfig.from_env())
    job = pykube.Job(api, pod_data)
    job.create()
