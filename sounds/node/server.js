'use strict';

const express = require('express');
const app = new express();

app.get('/', function(request, response){
    console.log('hello world');
    response.sendFile('static/index.html');
});
app.use('/static', express.static(__dirname + '/static'))
var server=app.listen(8081,function() {});