import kopf
import pykube
import yaml
import uuid

@kopf.on.resume("gdansk.harbor", "v1", "plays")
@kopf.on.create("gdansk.harbor", "v1", "plays")
def create(body, **kwargs):
    notes = body['spec']['notes'].lower()
    notes = notes.split(',')
    for note in notes:
        uid = str(uuid.uuid4())[:4]
        create_pod(note+uid)


def create_pod(note):
    pod_data = yaml.safe_load(f"""
        apiVersion: v1
        kind: Pod
        metadata:
          name: {note}
        spec:
          containers:
          - name: {note}
            image: alpine
            args: ['cat']
    """)

    # Actually create an object by requesting the Kubernetes API.
    api = pykube.HTTPClient(pykube.KubeConfig.from_env())
    pod = pykube.Pod(api, pod_data)
    pod.create()
    api.session.close()



