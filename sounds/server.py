#!/usr/bin/python3
from flask import Flask, make_response, render_template, jsonify
from flask import request
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

pod = []

@app.route('/pod', methods=['GET', 'POST'])
def get_pod():
    app.logger.info('pod: ' + str(pod))
    if request.method == 'POST':
        # content = request.json
        data = request.get_data()
        app.logger.info('data IN: ' + str(data))
        # app.logger.info('content IN: ' + str(content))
        # app.logger.info('IN: ' + str(content['name']))
        # pod.append(content['name'])
        pod.append(str(data).split('=')[1])
        return make_response('Test worked!',
                             200,)
    if request.method == 'GET':
        try:
            out = pod.pop()
        except IndexError:
            out = ''
        app.logger.info('OUT: ' + str(out))
        return jsonify({'name': out})



if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8080)
