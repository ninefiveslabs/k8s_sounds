import kopf


@kopf.on.create("", "v1", "pods")
def create(body, **kwargs):
    print(f"Create with spec: {body}")
    kopf.event(body, type="Important", reason="Action", message=f"Create with spec: {body['spec']}")



@kopf.on.update("", "v1", "pods")
def update(body, new, old, **kwargs):
    print(f"Update with spec: {body}")
    kopf.event(body, type="Important", reason="Action", message=f"Update\nNew: {new}"
                                                                f"\nOld: {old}")

